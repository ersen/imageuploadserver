package org.magnum.mobilecloud.image.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;


@Configuration
public class OAuth2Config {

    // @formatter:off
    @Configuration
    @EnableResourceServer
    protected static class ResourceServer extends ResourceServerConfigurerAdapter {

        @Override
        public void configure(HttpSecurity httpSecurity) throws Exception {
            //httpSecurity.csrf().disable().authorizeRequests().antMatchers("/protected/**").authenticated();
            
            httpSecurity.csrf().disable();
            
            httpSecurity.authorizeRequests().antMatchers("/oauth/token").anonymous();
            
            
            // If you were going to reuse this class in another
            // application, this is one of the key sections that you
            // would want to change
            
            // Require all GET requests to have client "read" scope
            httpSecurity.authorizeRequests().antMatchers(HttpMethod.GET, "/**").access("#oauth2.hasScope('read')");
            
            // Require all other requests to have "write" scope
            httpSecurity.authorizeRequests().antMatchers("/**").access("#oauth2.hasScope('write')").
            and().logout().logoutRequestMatcher(new AntPathRequestMatcher("/logout"));
        }
    }
    // @formatter:on

    @Configuration
    @EnableAuthorizationServer
    protected static class AuthorizationServer extends AuthorizationServerConfigurerAdapter implements EnvironmentAware {


        private static final String PROP_CLIENT_ID = "safran_app";
        private static final String PROP_SECRET = "safran_secret";

        @Bean
        public TokenStore tokenStore() {
            return new InMemoryTokenStore();
        }

        @Autowired
        @Qualifier("authenticationManagerBean")
        private AuthenticationManager authenticationManager;

        @Override
        public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
            endpoints.tokenStore(tokenStore()).authenticationManager(authenticationManager);
        }

        @Override
        public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
            clients.inMemory()
            .withClient(PROP_CLIENT_ID)
            .scopes("read", "write")
            .authorities("ROLE_ADMIN", "ROLE_USER")
            .authorizedGrantTypes("password")
            .secret(PROP_SECRET)
            .accessTokenValiditySeconds(1800);
        }

        @Override
        public void setEnvironment(Environment environment) {

        }
    }
}