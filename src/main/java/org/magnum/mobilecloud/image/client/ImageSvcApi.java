package org.magnum.mobilecloud.image.client;

import retrofit.client.Response;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.Part;
import retrofit.http.Streaming;
import retrofit.mime.TypedFile;

/**
 * This interface defines an API for a VideoSvc. The interface is used to provide a contract for client/server interactions. The interface is
 * annotated with Retrofit annotations so that clients can automatically convert the
 * 
 * 
 * @author ersen
 *
 */
public interface ImageSvcApi {

    public static final String DATA_PARAMETER = "data";
    
    public static final String TOKEN_PATH = "/oauth/token";

    public static final String IMAGE_SVC_PATH = "/image";
     
    public static final String FILTER_SVC_PATH = IMAGE_SVC_PATH + "/filter";

    public static final String VERIFY_LOGIN = IMAGE_SVC_PATH + "/welcome";

    @GET(VERIFY_LOGIN)
    public Boolean verifyLogin();
    
    @Streaming
    @Multipart
    @POST(FILTER_SVC_PATH)
    Response filterImage(@Part(DATA_PARAMETER) TypedFile videoData);
}
