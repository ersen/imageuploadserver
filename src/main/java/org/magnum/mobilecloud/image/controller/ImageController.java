
package org.magnum.mobilecloud.image.controller;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

import org.magnum.mobilecloud.image.client.ImageSvcApi;
import org.magnum.mobilecloud.image.conf.ImageConfig;
import org.magnum.mobilecloud.image.service.ImageFileManager;
import org.magnum.mobilecloud.image.worker.ImageWorker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class ImageController {

    private ImageFileManager imageManager;
    
    @Autowired
    private ImageConfig conf;
    
    @Autowired
    public ImageController(ImageFileManager imageManager) {
        this.imageManager = imageManager;
    }
    
    @RequestMapping(value = ImageSvcApi.VERIFY_LOGIN)
    public @ResponseBody Boolean verifyLogin() {
        return true;
    }

    @RequestMapping(value = "/image/filter", method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE,
            produces = MediaType.MULTIPART_FORM_DATA_VALUE)
    public @ResponseBody void filterImage(@RequestParam("type") String type,  @RequestPart("data") MultipartFile imgData, HttpServletResponse res) throws IOException, InterruptedException, ExecutionException {
        Path target = imageManager.saveImgData(imgData.getInputStream(), imgData.getOriginalFilename());
        
        String[] types = type.split(",");
        
        Future<?> future =  conf.taskExecutor().submit(new ImageWorker(target, new ArrayList<String>(Arrays.asList(types))));
        future.get();
        
        serveImage(imgData.getOriginalFilename(), imgData.getContentType(), res);
    }
    
    private ServletResponse serveImage(String imageName, String contentType, HttpServletResponse response) throws IOException {
        imageManager.copyImgData(imageName, response.getOutputStream());
        response.setStatus(200);
        response.addHeader("Content-Type", contentType);
        return response;
    }

}