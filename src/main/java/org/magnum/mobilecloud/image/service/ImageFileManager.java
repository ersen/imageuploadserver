/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License. You may obtain a
 * copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations
 * under the License.
 * 
 */
package org.magnum.mobilecloud.image.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

import org.springframework.stereotype.Service;

/**
 * This class provides a simple implementation to store video binary data on the file system in a "videos" folder. The class provides methods for
 * saving videos and retrieving their binary data.
 * 
 * @author ersen
 *
 */
@Service
public class ImageFileManager {

    private static final String DEFAULT_IMAGES_FOLDER = "images";

    private Path targetDir_;

    public ImageFileManager() throws IOException {
        this(DEFAULT_IMAGES_FOLDER);
    }

    public ImageFileManager(String dir) throws IOException {
        targetDir_ = Paths.get(dir);

        if (!Files.exists(targetDir_)) {
            Files.createDirectories(targetDir_);
        }
    }

    /**
     * This method copies the binary data for the given video to the provided output stream. The caller is responsible for ensuring that the specified
     * Video has binary data associated with it. If not, this method will throw a FileNotFoundException.
     * 
     * @param v
     * @param out
     * @throws IOException
     */
    public void copyImgData(String imageName, OutputStream out) throws IOException {
        Path source = getImagePath(imageName);
        if(!Files.exists(source)){
            throw new FileNotFoundException("Unable to find the referenced image file for name: "+imageName);
        }
        
        Files.copy(source, out);
    }
    
    private Path getImagePath(String imageName){
        assert(imageName != null);
        
        return targetDir_.resolve(imageName);
    }

    /**
     * This method reads all of the data in the provided InputStream and stores it on the file system. The data is associated with the Video object
     * that is provided by the caller.
     * 
     * @param imageData
     * @param name
     * @throws IOException
     */
    public Path saveImgData(InputStream imageData, String name) throws IOException {
        assert(imageData != null);

        Path target = getImagePath(name);
        Files.copy(imageData, target, StandardCopyOption.REPLACE_EXISTING);
        
        return target;
    }

}
