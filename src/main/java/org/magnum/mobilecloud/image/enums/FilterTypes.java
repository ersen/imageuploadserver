package org.magnum.mobilecloud.image.enums;


public enum FilterTypes {

    GRAY("org.marvinproject.image.color.grayScale.jar"),
    SEPIA("org.marvinproject.image.color.sepia.jar"),
    INVERT("org.marvinproject.image.color.invert.jar");

    public String jar;

    FilterTypes(String jar) {
        this.jar = jar;
    }

    public String getJar() {
        return jar;
    }
    
    public static String getJarByName(String name) {
        for(FilterTypes type: FilterTypes.values()) {
            if(type.name().equalsIgnoreCase(name))
                return type.getJar();
        } 
        return null;
    }
    
    
    
}
