package org.magnum.mobilecloud.image.enums;

import com.google.common.io.BaseEncoding;

public class Sample {

    public static void main(String[] args) {
        System.out.println(BaseEncoding.base64().encode(new String("safran_app" + ":" + "safran_secret").getBytes()));

    }

}
