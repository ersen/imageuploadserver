package org.magnum.mobilecloud.image.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value=HttpStatus.BAD_REQUEST, reason="No such filter")
public class InvalidFilterRequestException extends RuntimeException {

    private static final long serialVersionUID = 1L;

}
