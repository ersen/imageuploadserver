package org.magnum.mobilecloud.image.worker;

import java.nio.file.Path;
import java.util.List;

import org.magnum.mobilecloud.image.enums.FilterTypes;
import org.magnum.mobilecloud.image.exception.InvalidFilterRequestException;

import marvin.image.MarvinImage;
import marvin.io.MarvinImageIO;
import marvin.plugin.MarvinImagePlugin;
import marvin.util.MarvinPluginLoader;

public class ImageWorker implements  Runnable {

    private Path target;
    private List<String> types;
    
    public ImageWorker(Path target, List<String> types) {
        this.target = target;
        this.types = types;
    }
    
    @Override
    public void run() {
        filterImages(target, types);
    }
    
    public void filterImages(Path target, List<String> types) {
    	types.forEach(type -> filterImage(target, type));
    }
    
    public void filterImage(Path target, String type) {
        MarvinImage image = MarvinImageIO.loadImage("./" + target);
        
        String jar = FilterTypes.getJarByName(type);
        if(jar == null)
            throw new InvalidFilterRequestException();
        
        MarvinImagePlugin imagePlugin = MarvinPluginLoader.loadImagePlugin(jar);
        imagePlugin.process(image, image);
        image.update();
        
        MarvinImageIO.saveImage(image, "./" + target);
    }

} 
