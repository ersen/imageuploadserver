package org.magnum.mobilecloud.integration.test;

import java.io.File;
import java.util.UUID;

import org.magnum.mobilecloud.image.client.ImageSvcApi;
import org.magnum.mobilecloud.image.client.SecuredRestBuilder;

import retrofit.RestAdapter.LogLevel;
import retrofit.client.ApacheClient;

/**
 * 
 * The test requires that the VideoSvc be running first (see the directions in the README.md file for how to launch the Application).
 * 
 * To run this test, right-click on it in Eclipse and select "Run As"->"JUnit Test"
 * 
 * 
 * @author jules
 *
 */
public class VideoSvcClientApiTest {

    private final String USERNAME = "admin";
    private final String PASSWORD = "pass";
    private final String USERNAME2 = "user0";
    private final String PASSWORD2 = "pass";
    private final String CLIENT_ID = "mobile";
    private final String READ_ONLY_CLIENT_ID = "mobileReader";

    private final String TEST_URL = "https://localhost:8443";

    private File testVideoData = new File("src/test/resources/test.mp4");

    private ImageSvcApi videoSvc = new SecuredRestBuilder().setLoginEndpoint(TEST_URL + ImageSvcApi.TOKEN_PATH)
                                                           .setUsername(USERNAME)
                                                           .setPassword(PASSWORD)
                                                           .setClientId(CLIENT_ID)
                                                           .setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
                                                           .setEndpoint(TEST_URL)
                                                           .setLogLevel(LogLevel.FULL)
                                                           .build()
                                                           .create(ImageSvcApi.class);

    private ImageSvcApi videoSvc2 = new SecuredRestBuilder().setLoginEndpoint(TEST_URL + ImageSvcApi.TOKEN_PATH)
                                                            .setUsername(USERNAME2)
                                                            .setPassword(PASSWORD2)
                                                            .setClientId(CLIENT_ID)
                                                            .setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
                                                            .setEndpoint(TEST_URL)
                                                            .setLogLevel(LogLevel.FULL)
                                                            .build()
                                                            .create(ImageSvcApi.class);

    private ImageSvcApi invalidClientVideoService = new SecuredRestBuilder().setLoginEndpoint(TEST_URL + ImageSvcApi.TOKEN_PATH)
                                                                            .setUsername(UUID.randomUUID().toString())
                                                                            .setPassword(UUID.randomUUID().toString())
                                                                            .setClientId(UUID.randomUUID().toString())
                                                                            .setClient(new ApacheClient(UnsafeHttpsClient.createUnsafeClient()))
                                                                            .setEndpoint(TEST_URL)
                                                                            .setLogLevel(LogLevel.FULL)
                                                                            .build()
                                                                            .create(ImageSvcApi.class);


}
